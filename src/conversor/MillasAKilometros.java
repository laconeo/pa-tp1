package conversor;


import conversor.Conversor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 
 */
public class MillasAKilometros extends Conversor{

    private static final double UNA_MILLA = 1.60934;
    /**
     * Conversion de kilometro a milla
     * @param kilometros
     * @return 
     */
    @Override
    public Double convertirValor1Valor2(Double kilometros) {
        return kilometros * UNA_MILLA;
    }

    /**
     * Conversion de milla a kilometro
     * @param milla
     * @return 
     */
    @Override
    public Double convertirValo2Valor1(Double milla) {
        return milla / UNA_MILLA;
    }

    /**
     * Texto del label1 Millas
     * @return 
     */
    @Override
    public String getLabelValor1() {
        return "Millas";
    }
    /** 
     * Texto del label2 Kilometros
     * @return 
     */
    @Override
    public String getLabelValor2() {
        return "Kilometros";
    }

        /**
    * Titulo de combobox
    * @return tipo de conversión, millas a kilometros
    */  
    @Override
    public String toString() {
        return "Millas a Kilometros";
    }
    
    
}
