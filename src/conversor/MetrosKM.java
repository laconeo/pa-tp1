/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversor;

/**
 *
 * @author 
 */
public class MetrosKM extends Conversor {

    private static final double UN_KILOMETRO = 1000;
    
    /**
     * Convierte metros a kilometros
     * @param metros
     * @return 
     */
    @Override
    public Double convertirValor1Valor2(Double metros) {
        
        return metros / UN_KILOMETRO;
    }

    /**
     * Convierte kilometros a metros
     * @param km
     * @return 
     */
    @Override
    public Double convertirValo2Valor1(Double km) {
        return km * UN_KILOMETRO;
    }
    /**
     * Titulo del label 1
     * @return Metros
     */
    @Override
    public String getLabelValor1() {        
        return "Metros";
    }

    /**
    * Titulo de combobox
    * @return tipo de conversión, metros a kilometros
    */
    @Override
    public String toString() {
        return "Metros a Kilometros";
    }

    /**
     * Titulo del label 2
     * @return Kilometros
     */    
    @Override
    public String getLabelValor2() {
        return "Kilometros";
    }
    
}
