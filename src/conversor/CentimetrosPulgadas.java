/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversor;

/**
 *
 * @author 
 */
public class CentimetrosPulgadas extends Conversor {
    private static final double UNA_PULGADA = 2.54;

    /**
     * Convierte Centímetro a Pulgadas
     * @param centimetros valor a convertir
     * @return Pulgadas, igual a centímetros dividido por 2.54
     */
    @Override
    public Double convertirValor1Valor2(Double centimetros) {
        
        return centimetros / UNA_PULGADA;
    }
/**
 * Convierte de Pulgadas a centimetros
 * @param pulgadas valor a convertir
 * @return Centimetros, igual a centimetros multiplicado por 2.54
 */
    @Override
    public Double convertirValo2Valor1(Double pulgadas) {

        return pulgadas * UNA_PULGADA;
    }
/**
 * Titulo de combobox
 * @return tipo de conversión, centimetros a pulgadas
 */
    @Override
    public String toString() {
        return "Centímetros a Pulgadas";
    }
/**
 * Tiulo del label1
 * @return  Centimetros
 */
    @Override
    public String getLabelValor1() {
    
        return "Centímetros";
    }
/**
 * Titulo del labe2
 * @return  Pulgadas
 */
    
    @Override
    public String getLabelValor2() {
        return "Pulgadas";
    }
        
    
}
