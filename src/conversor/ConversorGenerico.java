/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversor;

import java.awt.HeadlessException;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author 
 */
public class ConversorGenerico extends javax.swing.JFrame {

    private boolean convertirAValor2;
    private Conversor conversorSeleccionado;
    private ArrayList<Conversor> conversores;
    /**
     * Creates new form Conversor
     */
    public ConversorGenerico() {
        initComponents();
        setTitle("Mi Conversor");
        this.setLocationRelativeTo(null);
        
        // Llenar combo
        conversores =  new ArrayList();
        conversores.add(new MetrosKM());
        conversores.add(new CentimetrosPulgadas());
        conversores.add(new GramosAKilos());
        conversores.add(new MillasAKilometros());
        
        for (Conversor conversor : conversores) {
            conversoresComboBox.
                    addItem(conversor.toString());
        }
        conversorSeleccionado = conversores.get(0);        
        setLabels();
    }
/**
 * Asignación automatica de labels
 */
    private void setLabels() {
        jLabel1.setText(conversorSeleccionado.getLabelValor1());        
        jLabel2.setText(conversorSeleccionado.getLabelValor2());
        valor1TextField.setText("");
        valor2TextField.setText("");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        valor1TextField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        aPulgadasButton = new javax.swing.JButton();
        valor2TextField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        conversoresComboBox = new javax.swing.JComboBox<String>();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        valor1TextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                valor1TextFieldFocusLost(evt);
            }
        });
        valor1TextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                valor1TextFieldKeyPressed(evt);
            }
        });

        jLabel1.setText("Valor1");

        aPulgadasButton.setText("Convertir");
        aPulgadasButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aPulgadasButtonActionPerformed(evt);
            }
        });

        valor2TextField.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                valor2TextFieldFocusLost(evt);
            }
        });
        valor2TextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                valor2TextFieldKeyPressed(evt);
            }
        });

        jLabel2.setText("Valor2");

        conversoresComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                conversoresComboBoxItemStateChanged(evt);
            }
        });
        conversoresComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                conversoresComboBoxActionPerformed(evt);
            }
        });

        jLabel3.setText("Seleccione Conversión");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(valor2TextField, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(203, 203, 203))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(valor1TextField, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(aPulgadasButton)
                                .addGap(54, 54, 54))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(41, 41, 41)
                        .addComponent(conversoresComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(54, 54, 54))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(conversoresComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addGap(39, 39, 39)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(valor1TextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addGap(24, 24, 24))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(aPulgadasButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(valor2TextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addContainerGap(39, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void aPulgadasButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aPulgadasButtonActionPerformed
        //System.out.println("Se presionó el botón a Pulgadas!!!");
        if (convertirAValor2) {
            convertirAValor2();
        } else {
            convertirAValor1();
        }


    }//GEN-LAST:event_aPulgadasButtonActionPerformed
    private void convertirAValor1() throws HeadlessException {
    // Reemplaza "," por "."
        String valor1String = valor2TextField.getText().replace(",", ".");
        
        // Verifico que el valor sea numerico
        
        Double valor1Double;
        try {
            // TODO reemplazar por lógica d o f
            if (valor1String.contains("F"))  {
                throw new NumberFormatException("Valor no numerico");                
            }
            else{
                if (valor1String.contains("f"))  {
                    throw new NumberFormatException("Valor no numerico");
                }
                else{
                     if (valor1String.contains("D"))  {
                        throw new NumberFormatException("Valor no numerico");
                    }
                     else{
                          if (valor1String.contains("d"))  {
                              throw new NumberFormatException("Valor no numerico");
                         }
                     }
                }
            }
            
            valor1Double = Double.valueOf(valor1String);
            
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this,
                    "No se pudo convetir ==> " + e.getMessage(),
                    "ERROR",
                    JOptionPane.ERROR_MESSAGE);
            
            valor2TextField.requestFocus();
            return;
        }
        Double valor2Double = 
                conversorSeleccionado.convertirValo2Valor1(valor1Double);
        valor1TextField.setText(String.format("%.2f", valor2Double));
    }
    
    
    private void convertirAValor2() throws HeadlessException {
        // Reemplaza "," por "."
        String valor1String = valor1TextField.getText().replace(",", ".");
        
        // Verifico que el valor sea numerico
        
        Double valor1Double;
        try {
            // TODO reemplazar por lógica d o f
            if (valor1String.contains("F"))  {
                throw new NumberFormatException("Valor no numerico");                
            }
            else{
                if (valor1String.contains("f"))  {
                    throw new NumberFormatException("Valor no numerico");
                }
                else{
                     if (valor1String.contains("D"))  {
                        throw new NumberFormatException("Valor no numerico");
                    }
                     else{
                          if (valor1String.contains("d"))  {
                              throw new NumberFormatException("Valor no numerico");
                         }
                     }
                }
            }
            
            valor1Double = Double.valueOf(valor1String);
            
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this,
                    "No se pudo convetir ==> " + e.getMessage(),
                    "ERROR",
                    JOptionPane.ERROR_MESSAGE);
            
            valor1TextField.requestFocus();
            return;
        }
        Double valor2Double = 
                conversorSeleccionado.convertirValor1Valor2(valor1Double);
        valor2TextField.setText(String.format("%.2f", valor2Double));
    }

    private void valor1TextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_valor1TextFieldFocusLost
        convertirAValor2 = true;
    }//GEN-LAST:event_valor1TextFieldFocusLost

    private void valor2TextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_valor2TextFieldFocusLost
        convertirAValor2 = false;
    }//GEN-LAST:event_valor2TextFieldFocusLost

    private void valor1TextFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_valor1TextFieldKeyPressed
        if (evt.getKeyChar()==KeyEvent.VK_ENTER) {
            convertirAValor2();
        }
    }//GEN-LAST:event_valor1TextFieldKeyPressed

    private void conversoresComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_conversoresComboBoxItemStateChanged
        // obtengo el indice del seleccionado
        int selectedIndex = 
                conversoresComboBox.getSelectedIndex();
        conversorSeleccionado = 
                conversores.get(selectedIndex);
        setLabels();
    }//GEN-LAST:event_conversoresComboBoxItemStateChanged

    private void conversoresComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_conversoresComboBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_conversoresComboBoxActionPerformed

    private void valor2TextFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_valor2TextFieldKeyPressed
        if (evt.getKeyChar()==KeyEvent.VK_ENTER) {
           convertirAValor1();
        }
    }//GEN-LAST:event_valor2TextFieldKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ConversorGenerico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ConversorGenerico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ConversorGenerico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ConversorGenerico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ConversorGenerico().setVisible(true);
            }
        });
    }

    private static final double UNA_PULGADA = 2.54;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton aPulgadasButton;
    private javax.swing.JComboBox<String> conversoresComboBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JTextField valor1TextField;
    private javax.swing.JTextField valor2TextField;
    // End of variables declaration//GEN-END:variables
}
