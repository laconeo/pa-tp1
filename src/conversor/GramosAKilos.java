/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversor;

/**
 *
 * @author 
 */
public class GramosAKilos extends Conversor{
    
    private static final double UN_KILO = 1000;
/**
 * Convierte de gramos a kilos
 * @param gramos
 * @return 
 */
    @Override
    public Double convertirValor1Valor2(Double gramos) {
        return gramos / UN_KILO;
    }

    
    /**
     * Convierte de kilos a gramos
     * @param kilo
     * @return 
     */
    @Override
    public Double convertirValo2Valor1(Double kilo) {
        return kilo * UN_KILO;
    }

    
    /**
     * Etiqueta label1 de gramos
     * @return 
     */
    @Override
    public String getLabelValor1() {
        return "Gramos";
    }
    
    /**
     * Etiqueta label2 de kilos
     * @return 
     */
    @Override
    public String getLabelValor2() {
        return "Kilos";
    }

    
    /**
    * Titulo de combobox
    * @return tipo de conversión, gramos a kilos
    */    
    @Override
    public String toString() {
        return "Gramos a Kilos";
    }
    
    
}
