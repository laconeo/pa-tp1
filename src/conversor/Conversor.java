/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversor;

/**
 *
 * @author 
 */
public abstract class Conversor {
    
    public abstract Double convertirValor1Valor2(Double valor1);
    public abstract Double convertirValo2Valor1(Double valor2);
    
    /**
     * Devuelve el Label del Valor 1
     * @return Valor 1
     */
    public abstract String getLabelValor1();
    
    /**
     * Devuelve el Label del Valor 2
     * @return  Valor 2
     */
    
    public abstract String getLabelValor2();
}
